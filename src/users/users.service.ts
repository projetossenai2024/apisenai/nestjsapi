import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { hash } from 'bcrypt';

@Injectable()
export class UsersService {
  private users: User[] = [];

  async create(createUserDto: CreateUserDto): Promise<User> {
    const saltOrRounds = 10;
    const passwordHashed = await hash(createUserDto.password, saltOrRounds);

    const user = {
      ...createUserDto,
      id: this.users.length + 1,
      password: passwordHashed,
  };

  this.users.push(user);

        console.log('passwordHashed', passwordHashed);
        

        return user;

  }

  async findAll(): Promise<User[]> {
    return this.users;
  }

  async findOne(id: number): Promise<User | undefined> {
    const user = this.users.find((user) => user.id === id);
    return user ? Promise.resolve(user) : Promise.resolve(undefined); 
  }

  async update(id: number, updateUserDto: UpdateUserDto): Promise<User | undefined> {
    // Find the existing user
    const userIndex = this.users.findIndex((user) => user.id === id);
    if (userIndex === -1) {
      return undefined; // User not found
    }
  
    // Update properties (with password hashing if needed)
    const user = this.users[userIndex];
    if (updateUserDto.password) {
      const saltOrRounds = 10;
      user.password = await hash(updateUserDto.password, saltOrRounds);
    }
    // Update other properties directly
    user.name = updateUserDto.name ?? user.name;
    user.email = updateUserDto.email ?? user.email;
  
    // Replace old user in the array with the updated one
    this.users[userIndex] = user; 
  
    return user;
  }

  async remove(id: number): Promise<boolean> {
    const userIndex = this.users.findIndex((user) => user.id === id);
    if (userIndex === -1) {
      return false; // User not found
    }
  
    this.users.splice(userIndex, 1); // Remove the user from the array
    return ;
  }
}
